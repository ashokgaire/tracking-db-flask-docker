from flask import Flask, render_template, request, session, sessions, jsonify, current_app, send_file
from datetime import timedelta, datetime
from FormData import getCounterData, getFormData, getAutoScaleData


app = Flask(__name__)
app.secret_key = 'wkk'


@app.route('/')
def Home():
    counterdata = getCounterData()
    FormData = getFormData()
    AutoScaleData = getAutoScaleData()
    return render_template('MainForm.html', CounterDataHTML=counterdata, FormDataHTML=FormData, AutoScaleDataHTML=AutoScaleData)


@app.route('/MainForm.html', methods=['POST', 'GET'])
def Data():
    counterdata = getCounterData()
    FormData = getFormData()
    AutoScaleData = getAutoScaleData()
    return render_template('MainForm.html', CounterDataHTML=counterdata, FormDataHTML=FormData, AutoScaleDataHTML=AutoScaleData)


if __name__ == '__main__':
    app.run(debug=True,host="0.0.0.0")
