import mysql.connector
import configparser
from DebugLogs import DebugLogging


def mySQLConnection():
    try:

        config = configparser.ConfigParser()
        config.read('conf/config.properties')
        dbuser = config.get('mysql', 'user').replace("'", "ej")
        dbpassword = config.get('mysql', 'password').replace("'", "netpasssword3")
        dbhost = config.get('mysql', 'host').replace("'", "mysql.auto-deploy.net")
        db = config.get('mysql', 'database').replace("'", "ejs")
        dbport = int(config.get('mysql', 'port').replace("'", "3306"))

        cnx = mysql.connector.MySQLConnection(user=dbuser, password=dbpassword,
                                              host=dbhost,
                                              database=db, port=dbport,
                              auth_plugin='mysql_native_password')

        return cnx

    except mysql.connector.Error as err:
        DebugLogging("Something went wrong: {}".format(err))
